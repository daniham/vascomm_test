@extends('master')

@section('konten')
<h3>Tampil Data Pelanggan</h3>
<a class="btn btn-success" href="{{route('register')}}"><i class="fa fa-plus"></i> Tambah Pelanggan</a><br><br>
<table class="table table-bordered table-hover">
  <tr>
    <th>#ID</th>
    <th>Username</th>
    <th>Email</th>
    <th>Role</th>
    <th>Active</th>
    <!-- <th>image</th> -->
    <th>Aksi</th>
  </tr>
  @foreach($user as $s) 
  <tr>
    <td>{{$s->id}}</td>
    <td>{{$s->name}}</td>
    <td>{{$s->email}}</td>
    <td>
    @if ($s->role === 0)
        Admin
    @else
        Pelanggan
    @endif
    </td>
    <td>
        @if ($s->active === 1)
             Aktif
        @else
            Tidak Aktif
        @endif
        
    </td>
    <!-- <td>{{$s->no_hp}}</td> -->
    <td>
      <a href="/pelanggan/ubah/{{$s->id}}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"> </i>Aprove status</a>
      <a href="/pelanggan/hapus/{{$s->id}}" onclick="return confirm('Apakah Anda Yakin Menghapus Data?');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</a>
    </td>
  </tr>
  @endforeach
</table>
@endsection


