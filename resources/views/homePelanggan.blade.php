@extends('master')

@section('konten')
  <h4>Selamat Datang <b>{{Auth::user()->name}}</b>, Anda Login sebagai <b>
  @if (Auth::user()->role === 0)
    Admin
  @else
      Pelanggan
  @endif
  
  </b>.</h4>
@endsection