@extends('master')

@section('konten')
<h3>Ubah Data Pelanggan</h3>
  @foreach($user as $s)
    <form method="post" action="{{route('updateuser')}}">
      @csrf
      <input type="hidden" name="id" value="{{$s->id}}">
      <div class="form-group">
        <label><i class="fa fa-envelope"></i> Email</label>
        <input type="email" name="email" value="{{$s->email}}" class="form-control" placeholder="Email" required="">
      </div>
      <div class="form-group">
        <label><i class="fa fa-user"></i> Username</label>
        <input type="text" name="name" value="{{$s->name}}" class="form-control" placeholder="Nama Pelanggan" required="">
      </div>

      <div class="form-group">
            <strong><i class="fa fa-address-book"></i>Status Pelanggan:</strong>
                <select name="active" id="active" class="form-control">
                    <option value="">Status Pelanggan</option>
                    <option value="0">Tidak Aktif</option>
                    <option value="1">Aktif</option>
                </select>
                @error('active')
                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
        </div>
     
      <div class="form-group text-right">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update Data</button>
      </div>
    </form>
  @endforeach
@endsection