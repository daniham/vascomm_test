<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Models\User;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function pelanggan()
    {
        return view('homePelanggan');
    }

    public function tampilpelanggan()
    {
    $user = User::select('*')
                ->get();
    return view('tampilpelanggan', ['user' => $user]);
    }

    public function ubahuser($id)
    {
       $user = User::select('*')
                 ->where('id', $id)
                 ->get();
    
       return view('ubahuser', ['user' => $user]);
    }
    
    public function updateuser(Request $request)
    {
       $santri = User::where('id', $request->id)
                 ->update([
                    'name' => $request->name,
                    'email' => $request->email,
                    // 'password' => Hash::make($request->password),
                    // 'role' => $request->role,
                    'active' => 1,
                    // 'image' =>$file->getClientOriginalName()
                 ]);
    
       return redirect()->route('tampilpelanggan');
    }
    public function hapuspelanggan($id)
    {
        $user = User::where('id', $id)
                ->delete();

        return redirect()->route('tampilpelanggan');
    }
}