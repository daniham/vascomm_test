<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Session;

class LoginController extends Controller
{
    public function login()
    {
        if (Auth::check()) {
            return redirect('home');
        }else{
            return view('login');
        }
    }

    public function actionlogin(Request $request)
    {
        $data = [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];

        // $pegawai = DB::table('users')->get();
        // $users = DB::table('users')->get();
        $users = DB::table('users')->where('email', $request->input('email'))->first();
       
            if($users->role == 0){
                if (Auth::Attempt($data)) {
                    return redirect('home');
                }else{
                    Session::flash('error', 'Email atau Password Salah');
                    return redirect('/');
                }
            }else{
                if (Auth::Attempt($data)) {
                    return redirect('pelanggan');
                }else{
                    Session::flash('error', 'Email atau Password Salah');
                    return redirect('/');
                }
            }
    
    }

    public function actionlogout()
    {
        Auth::logout();
        return redirect('/');
    }
}