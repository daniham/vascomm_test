<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Session;


class RegisterController extends Controller
{
    public function register()
    {
        return view('register');
    }
    

    public function actionregister(Request $request)
    {
       
        $this->validate($request, [
            'image' => 'required',
        ]);
     
        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('image');
     
        $tujuan_upload = 'data_file';
        
        $file->move($tujuan_upload,$file->getClientOriginalName());

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => $request->role,
            'active' => 0,
            'image' =>$file->getClientOriginalName()

        ]);

        Session::flash('message', 'Register Berhasil. Akun Anda sudah Aktif silahkan Login menggunakan username dan password.');
        return redirect('register');
    }
}