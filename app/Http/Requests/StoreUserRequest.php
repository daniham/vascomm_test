<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:100'],
            'email' => ['required', 'max:100'],
            'password' => ['required', 'max:100'],
            'role' => ['required', 'max:100'],
            'active' => ['required', 'numeric', 'min:1'],
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ];
    }
}
